-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 16 Jul 2019 pada 07.36
-- Versi server: 10.1.36-MariaDB
-- Versi PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `les`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_murid`
--

CREATE TABLE `data_murid` (
  `nama_murid` varchar(50) NOT NULL,
  `kota` varchar(45) DEFAULT NULL,
  `jenkel` varchar(45) DEFAULT NULL,
  `agama` varchar(45) DEFAULT NULL,
  `kelas` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_murid`
--

INSERT INTO `data_murid` (`nama_murid`, `kota`, `jenkel`, `agama`, `kelas`) VALUES
('Dono', 'Banten', 'Laki-Laki', 'Islam', 10),
('Handika', 'Mataram', 'Laki-Laki', 'Kristen', 12),
('Hartono', 'Banjarmasin', 'Laki-Laki', 'Islam', 10),
('Hendra', 'Jakarta', 'Laki-Laki', 'Kristen', 10),
('Hesvi', 'Medan', 'Perempuan', 'islam', 11),
('Ilham', 'Jambi', 'Laki-Laki', 'islam', 10),
('Manda', 'Bekasi', 'Laki-Laki', 'Katolik', 12),
('Putri', 'Maria', 'Perempuan', 'Islam', 12),
('Raditya', 'Depok', 'Laki-Laki', 'Islam', 11),
('Rizky', 'Bekasi', 'Laki-Laki', 'Islam', 10),
('Sinta', 'Banjarmasin', 'Perempuan', 'Islam', 11),
('Siska', 'Tokyo', 'Perempuan', 'Katolik', 11),
('Vela', 'Jakarta', 'Perempuan', 'Islam', 11);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengajar`
--

CREATE TABLE `pengajar` (
  `nama_pengajar` varchar(50) NOT NULL,
  `no_hp` varchar(30) NOT NULL,
  `jenkel` varchar(10) DEFAULT NULL,
  `agama` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengajar`
--

INSERT INTO `pengajar` (`nama_pengajar`, `no_hp`, `jenkel`, `agama`) VALUES
('Eki Suryadi', '08546685566', 'Laki-laki', 'Islam'),
('Hartini', '08556119879', 'Perempuan', 'Kristen'),
('Hendika', '087987697658', 'Laki-Laki', 'Islam'),
('Mariyadi', '084569874565', 'Laki-laki', 'katolik');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `data_murid`
--
ALTER TABLE `data_murid`
  ADD PRIMARY KEY (`nama_murid`);

--
-- Indeks untuk tabel `pengajar`
--
ALTER TABLE `pengajar`
  ADD PRIMARY KEY (`nama_pengajar`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
